#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sklearn.cluster import KMeans
import numpy as np
import cv2
import glob
import sys
import time
import math

class checkerboardData :
	def __init__(self):
		self.isPictureTaken = False #Quand True on a accés à l'image, à l'image en niveaux de gris, et au format de l'image
		self.picture = 0
		self.grayPicture = 0
		
		self.isCheckerboardVisible = False #Quand True on a accès aux coordonnées dans l'espace d'image des intersections entre cases, 
										   # et au centre de ces coordonnées
		self.points = []
		self.center = (-1,-1)			
		self.transformationMatrix = 0	   # et a la matrice de transformation géométrique
		self.extremities = []			  #Position des extrémités 


		self.isCheckerboardColorParametersCalculated = False 	#Quand True, on a accès aux couleurs "moyennes" des pions des deux camps, et des cellules des deux camps
		self.cellColor = ((-1,-1,-1),(-1,-1,-1))				
		
		self.isPawnsColorParametersCalculated = False			#ainsi qu'aux parametres qui permettent de différencier les pions des deux camps
		self.pawnsColor = ((-1,-1,-1),(-1,-1,-1))

		self.sizeX = 300
		self.sizeY = 300
		self.channels = 3

class visionUtilities:
	def __init__(self, videoCaptureInputName=0):
		self.camera = cv2.VideoCapture()
		self.camera.open(videoCaptureInputName)
		if not self.camera.isOpened():
			print("Impossible d'ouvrir la caméra. Vérifier le branchement.")
		self.currentData = checkerboardData() 
		self.currentCheckerboardData = []
		self.checkerboardDatas = {}
		self.checkerboardFormat = (9,9)
		self.camera.set(cv2.CAP_PROP_AUTO_EXPOSURE  , 0)
		time.sleep(2)

		#Calibration de la caméra

		save = np.load("calibrationFile.txt")
		ret = save['arr_0']
		mtx = save['arr_1']
		dist = save['arr_2']
		rvecs = save['arr_3']
		tvecs = save['arr_4']
		ret, img = self.camera.read()
		h,  w = img.shape[:2]
		newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0,(w,h))
		self.mapx,self.mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)
		dst = cv2.remap(img,self.mapx,self.mapy,cv2.INTER_LINEAR)

	# ==== Sans rapport avec le jeu ====
	#Fonction de récupération d'image de la caméra avec possibilité de pré traitement 
	#Private
	def getPicture(self, hist_eq=True):
		if not self.camera.isOpened():
			return (False, None)
		ret, picture = self.camera.read()
		picture = cv2.remap(picture, self.mapx, self.mapy, cv2.INTER_LINEAR)

		if ret == True and hist_eq == True:
			picture = self.hisEqulColor(picture)

		return (ret, picture)

	def takePicture(self):
		self.currentData = checkerboardData() 
		ret, self.currentData.picture = self.getPicture()
		if ret == False:
			return False

		if ret:
			self.currentData.isPictureTaken = True
			self.currentData.grayPicture = cv2.cvtColor( self.currentData.picture, cv2.COLOR_BGR2GRAY)
		return ret

	#Retourne un tuple de la taille de l'image
	#(x,y,channels de couleurs)
	def getPictureFormat(self):
		if self.currentData.isPictureTaken == True:
			return self.currentData.picture.shape
		return (-1,-1,-1)

	# ==== Avant pose des pions ====
	#Trouve le damier
	#Ne retourne rien
	def findCheckerboard(self):
		if  self.currentData.isPictureTaken == True:
			criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
			ret, self.currentData.points = cv2.findChessboardCorners(self.currentData.grayPicture, self.checkerboardFormat, None)
			if ret:
				self.currentData.points = cv2.cornerSubPix(self.currentData.grayPicture,self.currentData.points,(11,11),(-1,-1),criteria)
			return ret
		return False

	#Calcule les différents trucs, dans le but de les utiliser direct (ca reste dans "currentCheckerboardData") ou de les sauvegarder
	def calculateCheckerboardData(self):
		if self.currentData.isPictureTaken == False:
			return
		if self.currentData.points is None or len(self.currentData.points) < 81:
			return
		#Calcul des coordonées des points

		center = [0,0]
		for i in range(0, len(self.currentData.points)):
			center[0] += self.currentData.points[i][0][0];
			center[1] += self.currentData.points[i][0][1];
		center[0] = center[0] / len(self.currentData.points)
		center[1] = center[1] / len(self.currentData.points)

		if len(self.currentData.points) > 0:
			a = self.currentData.points[0][0]
			b = self.currentData.points[8][0]
			c = self.currentData.points[72][0]
			d = self.currentData.points[80][0]
			print(a)
			print(b)
			adx = math.sqrt((a[0]-d[0])**2)
			ady = math.sqrt((a[1]-d[1])**2)
			bcx = math.sqrt((c[0]-b[0])**2)
			bcy = math.sqrt((c[1]-b[1])**2)

			self.currentData.center = (center[0]/len(self.currentData.points), center[1]/len(self.currentData.points))
			self.currentData.sizeX = 400
			self.currentData.sizeY = 400
			xs8 = int(self.currentData.sizeX/8)
			ys8 = int(self.currentData.sizeY/8)
			self.currentData.channels = 3

			pts1 = np.float32([a, b, c, d])
			pts2 = np.float32([[xs8+0,ys8+0],[xs8+self.currentData.sizeX,ys8+0],[xs8+0,ys8+self.currentData.sizeY ],[xs8+self.currentData.sizeX,ys8+self.currentData.sizeY]])
			
			self.currentData.extremities = pts1
			self.currentData.transformationMatrix = cv2.getPerspectiveTransform(pts1,pts2)
			self.currentData.isCheckerboardVisible = True


	#Enregistre les infos sur l'échiquier. Ca permet de les réutiliser quand l'échiquier ne sera plus visible 
	#(comme on connait précisément la position du bras)
	#Sauvegarde : 
	# - Une image
	# - Une liste de points
	#Le nom ne peut pas etre "CUSTOM"
	def saveCheckerboardData(self, name):
		self.currentCheckerboardData[name] = self.currentData

	def loadCheckerboardData(self, name):
		self.currentData = self.currentCheckerboardData[name]

	def getCheckerboardData(self):
		return self.currentData

	def getBoundsDistances(self): #Valeur = 0, dans les boundaries. Valeur < 0, hors des boundaries par la gauche ou le haut. Valeur > 0 hors des bnd par droite / bas
		boundsDistances = np.array([])
		for pt in self.currentData.extremities:
			a = 0
			b = 0
			if pt[0] - self.getPictureFormat()[1]>0:
				a = pt[0] - self.getPictureFormat()[1]
			elif pt[0] < 0:
				a = pt[0]
			if pt[1]-self.getPictureFormat()[0]>0:
				b = pt[1]-self.getPictureFormat()[0]
			elif pt[1] < 0:
				b = pt[1]
			boundsDistances = np.append(boundsDistances, [a,b], axis=0)
		return boundsDistances

	def transform(self, image, M, sizeX, sizeY):
		if M is None:
			return image 
		try:
			dst = cv2.warpPerspective(image,M,(int(sizeX/8*2) + sizeX,int(sizeY/8*2)+sizeY))
			return dst
		except:
			return image
		return dst
		
	#Permet de récupérer l'image stockée à name. 
	#Si flatten = True, l'image sera applatie de facon a voir l'échiquier à plat
	#Si les deux sont true, on aura une image d'échiquier plat
	def getCheckerboardPicture(self, flatten=False):
		if self.currentData.isPictureTaken:
			image = self.currentData.picture
			if flatten == True:
				if self.currentData.isCheckerboardVisible:
					image = self.transform(image,self.currentData.transformationMatrix,self.currentData.sizeX,self.currentData.sizeY )
			return image
 
	def centroid_histogram(self,clt):
		# grab the number of different clusters and create a histogram
		# based on the number of pixels assigned to each cluster
		numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
		(hist, _) = np.histogram(clt.labels_, bins = numLabels)
		# normalize the histogram, such that it sums to one
		hist = hist.astype("float")
		hist /= hist.sum()
		# return the histogram
		return hist

	def plot_colors(self, hist, centroids):
		# initialize the bar chart representing the relative frequency
		# of each of the colors
		bar = np.zeros((50, 300, 3), dtype = "uint8")
		startX = 0
		# loop over the percentage of each cluster and the color of
		# each cluster
		for (percent, color) in zip(hist, centroids):
			# plot the relative percentage of each cluster
			endX = startX + (percent * 300)
			cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
				color.astype("uint8").tolist(), -1)
			startX = endX
		# return the bar chart
		return bar


	# ==== Utile après pose des pions ====
	def displayWithWriting(self, image, text, coord=(1,10)):
		fontFace               = cv2.FONT_HERSHEY_SIMPLEX
		bottomLeftCornerOfText = coord
		fontScale              = 1
		fontColor              = (0,255,0)
		cv2.putText(image,str(text), bottomLeftCornerOfText, fontFace,fontScale,fontColor)
		cv2.imshow('img', image)

	#Egalisation d'histogramme en couleurs (on préfère utiliser l'égalisation adaptative (clahe))
	#Private
	def hisEqulColor(self, img):
		ycrcb=cv2.cvtColor(img,cv2.COLOR_BGR2Lab)
		#ycrcb=cv2.cvtColor(img,cv2.COLOR_BGR2YCR_CB)
		channels=cv2.split(ycrcb)
		clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
		channels[0] = clahe.apply(channels[0])
		cv2.merge(channels,ycrcb)
		cv2.cvtColor(ycrcb,cv2.COLOR_Lab2BGR,img)
		return img

	def calculatePawnsColorParameters(self):
		x=0
		y=0
		val = 0
		while True:
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
			cv2.imshow('Not eq',self.getGamePicture(True, False, False))
			cv2.imshow('Eq',self.getGamePicture(True, False, True))
			img = self.getGamePicture(True, False, True)
			img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			img = cv2.medianBlur(img,5)
			img = cv2.medianBlur(img,5)
			img = cv2.medianBlur(img,5)
			img = cv2.medianBlur(img,5)

			cimg = self.getGamePicture(True, False, True)
			circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20,param1=200,param2=15,minRadius=10,maxRadius=20)

			if not (circles is None):
				circles = np.uint16(np.around(circles))
				for i in circles[0,:]:
				    # draw the outer circle
				    cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
				    # draw the center of the circle
				    cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)

			cv2.imshow('detected circles',cimg)
		for x in range(0,10):
				for y in range(0,10):
					currentCell = self.getCell(x, y)
					# currentCell = cv2.cv2.bilateralFilter(currentCell,9,75,75)
					# print("var=")
					# print(np.var(currentCell))
					# if(np.var(currentCell) > 1000):
					while True:
						if cv2.waitKey(1) & 0xFF == ord('q'):
							break
						currentCell = self.getCell(x, y)
						#currentCell = cv2.cv2.bilateralFilter(currentCell,9,75,75)
						cv2.imshow('detected circles',currentCell)
					# while True:
					# 	if cv2.waitKey(1) & 0xFF == ord('q'):
					# 		break
					# 	currentCell = self.getCell(x, y)
					# 	cv2.imshow('current cell', currentCell)
					# 	cv2.imshow('board', self.getGamePicture(True, False, True))
					# 	print("var=")
					# 	print(np.var(currentCell))

					# 	img = cv2.cvtColor(self.getCell(x, y), cv2.COLOR_BGR2GRAY)
					# 	img = cv2.medianBlur(img,5)
					# 	cimg = self.getCell(x, y)
						
					# 	circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20,
					# 	                            param1=70,param2=20,minRadius=0,maxRadius=0)
					# 	if not (circles is None):
					# 		circles = np.uint16(np.around(circles))
					# 		for i in circles[0,:]:
					# 		    # draw the outer circle
					# 		    cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
					# 		    # draw the center of the circle
					# 		    cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)

					# 	cv2.imshow('detected circles',cimg)

					# a = cv2.cvtColor(currentCell, cv2.COLOR_BGR2HSV)
					# a2 =  a[:,:,1]
					# a3 =  a[:,:,2]
					# a2B = cv2.GaussianBlur(a2,(11,11),0)
					# cv2.imshow('2', a2)
					# cv2.imshow('3',a3)
					# cv2.imshow('2BLURED', a2B)
					# cv2.imshow('2BT', cv2.threshold(a,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU))

					# #cc = cv2.cvtColor(currentCell, cv2.COLOR_BGR2GRAY)

					# #blur = cc#cv2.GaussianBlur(cc,(11,11),0)
					# #ret3,a = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
					# currentCell = self.getCell(x, y) 
					# currentCell = cv2.cvtColor(currentCell, cv2.COLOR_BGR2HSV)
					# currentCell = currentCell[:,:,0] #currentCell.reshape((currentCell.shape[0]*currentCell.shape[1], 3))
					# #currentCell = currentCell[:,:,2]

					# currentClusterizer = KMeans(n_clusters = 2)
					# currentClusterizer.fit(currentCell)
					# cellSizeX = self.currentData.sizeX/10
					# cellSizeY = self.currentData.sizeY/10
					
					# aa= currentClusterizer.cluster_centers_[0]
					# bb =  self.getCell(x, y) 
					# cv2.rectangle( bb,(0,0), (10,10),(int(aa[0]), int(aa[1]), int(aa[2])),3)
					# aa = currentClusterizer.cluster_centers_[1]
					# cv2.rectangle(bb,(15,0),(25,10),(int(aa[0]), int(aa[1]), int(aa[2])),3)
					# cv2.imshow('pouet', bb)
					# print("var=")
					# print(np.var(a3))


					# while True:
					# 	if cv2.waitKey(1) & 0xFF == ord('q'):
					# 		break
					# #a = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 115, 1)
					# #cv2.imshow('aaa', a)
					# #currentCell =  cv2.cvtColor(self.currentData.picture, cv2.COLOR_BGR2HSV)
					# #self.displayWithWriting(self.getGamePicture(True, False), currentClusterizer.cluster_centers_, (int(x)*int(cellSizeX),int(y+2)*int(cellSizeY)))
					# #a = math.sqrt((currentClusterizer.cluster_centers_[0][0]-currentClusterizer.cluster_centers_[1][0])**2+(currentClusterizer.cluster_centers_[0][1]-currentClusterizer.cluster_centers_[1][1])**2+(currentClusterizer.cluster_centers_[0][1]-currentClusterizer.cluster_centers_[1][1])**2)
					# #print(a)
					# # val += a
		val = val/81

	#Cette fonction effectue une prise de l'image du jeu et la renvoie.
	#Si flatten = True, l'image sera applatie par rapport à la transformation dans currentData
	#Si gray = True, l'image est en noir et blanc
	#Si hist_eq = True, on effectue une egalisation d'histogramme
	#Permet d'avoir une image du jeu à plat
	#Si name='' on ignore crop ou flatten (logique)
	def getGamePicture(self, flatten=False, gray=False, hist_eq=False):
		ret, image = self.getPicture(hist_eq)
		if flatten == True :
			image = self.transform(image, self.currentData.transformationMatrix, self.currentData.sizeX ,self.currentData.sizeY )

		if gray == True:
			image  = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

		return image
	
	#<espace damier> // Ces fonctions fournissent des infos utiles au moteur d'échec (en espace damier), pas pour l'affichage
	#private	
	#DOIT SE BASER SUR CHECKERBOARD FINDER 
	def getCell(self, x, y):
		cellSizeX = self.currentData.sizeX/10
		cellSizeY = self.currentData.sizeY/10
		return self.getGamePicture(True, False, True)[int(x*cellSizeX)+2:int((x+1)*cellSizeX)-2,  int(y *cellSizeY)+2:int((y+1)*cellSizeY)-2,:]
	#private
	def whoseOnCell(self, x, y):
		pass
	#</espace damier>

	#En dessous vv fournis des infos utiles au bras robot
	#<passage d'un espace à l'autre>
	#Donne la position dans l'espace image du pion se trouvant sur la case aux coordonnées en espace damier fournies.
	#retourne rien si y'a pas de pion dans la case demandée
	def getPawnImagePositionFromCheckerboardPosition(self, x, y):
		pass
	#</passage d'un espace à l'autre>
	#<espace image>
	#Retourne les barycentres dans l'image (2D) des pions
	#Ils seront calculés par rapport à getGamePicture(name, flatten)
	def getPawnsPositions(self, flatten=False, customBoardPosition=None):
		pass
	#</espace image>


	def getPawnsNumber(self):
		pass

	#Retourne un tuple de couleurs des pions (pionFonce, pionClair)
	def getPawnsColors(self):
		pass

	

mU = visionUtilities()
print("- getPIctureFormat : ")
mU.takePicture()
print(mU.getPictureFormat())
mU.findCheckerboard()
mU.calculateCheckerboardData()
print(mU.getCheckerboardData().isCheckerboardVisible)
cv2.imshow('Echiquier applati', mU.getCheckerboardPicture(True))
print(mU.getBoundsDistances())


mU.calculatePawnsColorParameters()
mU.getCell( 0,0)

cv2.destroyAllWindows()
mU.camera.release()