#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import glob
import sys
import time
#Permet de trouver un échiquier dans l'image en noir et blanc
#gray : image en noir et blanc
#checkerBoard : tuple de la taille de l'échiquier à trouver (x-1, y-1)


class CheckerBoardCalibration:

    def __init__(self, checkerBoardFormat):
        #Quasi constantes
        self.checkerBoardFormat = checkerBoardFormat
        self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        #To be filled
        self.objpoints = []
        self.imgpoints = []
        self.corners = []

    def findCheckerboard(self, gray):
        ret, self.corners = cv2.findChessboardCorners(gray, self.checkerBoardFormat, None)
        return ret, corners

    def getObjPoints(self, gray):
        
        if self.corners == []:
            ret, corn = self.findCheckerboard(gray)
            if not ret:
                print("ERROR ! NO RET")
        objp = np.zeros((self.checkerBoardFormat[0]*self.checkerBoardFormat[1],3), np.float32)
        objp[:,:2] = np.mgrid[0:self.checkerBoardFormat[0],0:self.checkerBoardFormat[1]].T.reshape(-1,2)
        imgpcorners = cv2.cornerSubPix(gray,self.corners,(11,11),(-1,-1),self.criteria)
        self.objpoints.append(objp)
        self.imgpoints.append(imgpcorners)

    def getCalibration(self, gray):
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(self.objpoints, self.imgpoints, gray.shape[::-1], None, None)
        return ret, mtx, dist, rvecs, tvecs

    def drawCheckerboard (self, img):
        img = cv2.drawChessboardCorners(img, self.checkerBoardFormat, self.corners,True)
        cv2.imshow('img',img)


# def findCheckerboard(gray, checkerBoard):
#     # Find the chess board corners
#     ret, corners = cv2.findChessboardCorners(gray, checkerBoard,None)
#     return ret, corners

# #Récupere les infos de calibration de la caméra à partir d'une image contenant un échiquier,
# #et de la liste de ses coins, ainsi que de sa taille
# def getCalibration(corners, gray, checkerBoard):
#     criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

#     objp = np.zeros((checkerBoard[0]*checkerBoard[1],3), np.float32)
#     objp[:,:2] = np.mgrid[0:checkerBoard[0],0:checkerBoard[1]].T.reshape(-1,2)

#     objpoints = []
#     imgpoints = []

#     objpoints.append(objp)
    
#     corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
#     imgpoints.append(corners2)
    
#     ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
#     return ret, mtx, dist, rvecs, tvecs

# #Permet d'afficher l'échiquier trouvé
# def drawCheckerboard (img, checkerBoard, corners):
#     img = cv2.drawChessboardCorners(img, checkerBoard, corners,True)
#     cv2.imshow('img',img)

#Calibration manuelle
if __name__ == '__main__':
    cap = cv2.VideoCapture(0)
    myCalibrator = CheckerBoardCalibration((9,9))
    save_file = open("camCalib.txt", "wb")
    a = 0
    corners = []
    print("Veuillez placer un damier 10x10 dans le champ de la caméra")
    while(True):
        while a < 5 :
            ret, img = cap.read()
            cv2.imshow('img', img)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            ret, cc = myCalibrator.findCheckerboard(gray)
            if ret:
                print("Found")
                myCalibrator.getObjPoints(gray)
                a=a+1
                ret = False
                time.sleep(1)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                cap.release()
                sys.exit()

        ret, mtx, dist, rvecs, tvecs = myCalibrator.getCalibration(gray)
        myCalibrator.drawCheckerboard(img)

        print("Calibrated")
        print(ret)
        print(mtx)
        print(dist)
        print(rvecs)
        print(tvecs)
        print("Si l'image est incorrecte, quittez avec q et relancez le programme")
        print("Si l'image est correcte, appuyez sur o pour valider et quitter.")

        while(True):
            ret, img = cap.read()
            h,  w = img.shape[:2]
            newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0,(w,h))

            mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)
            dst = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)
            x,y,w,h = roi
            
            cv2.imshow('Image corrigée', dst)
            pressed = cv2.waitKey(1) & 0xFF
            if pressed == ord('o'):
                np.save(save_file,ret,False)
                np.save(save_file,mtx,False)
                np.save(save_file,dist,False)
                np.save(save_file,rvecs,False)
                np.save(save_file,tvecs,False)
                cv2.destroyAllWindows()
                cap.release()
                sys.exit()                               
                        
            if pressed == ord('q'):
                cv2.destroyAllWindows()
                cap.release()
                sys.exit()
        
