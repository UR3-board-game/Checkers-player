import serial
import serial.tools.list_ports
import time

class Pince:
	def __init__(self, name):
		self._name = name
	def check(self, link):
		pass
	def open(value):
		pass
	def close(value):
		pass

class Ventouz(Pince):
	def __init__(self, name, speed=9600, port=""):
		Pince.__init__(self, name)

		#Detection automatique du port
		if port == "":
			portList = serial.tools.list_ports.comports()
			for currentPort in portList:
				if currentPort[2] != "n/a":
					serialLink = self.tryStart(speed,currentPort[0])						
					if serialLink:
						if self.check(serialLink):
							self._serialLink = serialLink
							break
						else:
							print("Incorrect check")
					else:
						print("Impossible to connect to port")
		else:
			serialLink = self.tryStart(speed, port)
			if serialLink:
				if self.check(serialLink):
					self._serialLink = serialLink

	def open(self, value):
		self.send("0\n")

	def close(self, value):
		self.send("1\n")

	def contact(self):
		self._serialLink.write("?\n")
		return self._serialLink.realdine()

	#Attend que le capteur de contact touche
	def waitForContact(self, function, parameter, timeout=1000):
		start = time.time()
		while time.time() - start < timeout:
			if self._serialLink.read(1) == "1":
				function(parameter)
				return
	#Attend un etat particulier du capteur de contact
	def waitForState(self, function, parameter, state,  timeout=1000):
		start = time.time()
		while time.time() - start < timeout:
			if self._serialLink.read(1) == state:
				function(parameter)
				return


	# *--- Private ---*

	def check(self, link):
		link.write("#hello\n")
		value = link.readline()
		print(value)
		if value.startswith(self._name):
			return True
		return False
	def tryStart(self, speed, port):
		serialLink = serial.Serial()
		serialLink.baudrate = speed
		serialLink.port = port
		serialLink.timeout = .5
		serialLink.open()
		if not serialLink.isOpen():
			return None
		else:
			time.sleep(2)
			serialLink.flushOutput()
			serialLink.flushInput()
			return serialLink

	def send(self,message):
		self._serialLink.write(message)
		self._serialLink.flush()
		self._serialLink.flushOutput()


if __name__ == "__main__":
	def b(a):
		print("execute")
	a  = Ventouz("ventouz",115200)
	a.waitForContact(b, None)
