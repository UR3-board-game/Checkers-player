#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import math3d as m3d
import math
import urx_modif

from Pince import Ventouz
ip = "192.168.0.152"

# Bidouille et tests 


def stop(robot):
	robot.speedl([0,0,0,0,0,0], 2,1)

def emergencyStop(robot):
	robot.speedl([0,0,0,0,0,0], 5,1)

def moveDown(robot, maxtime, speed=0.05):
	if speed > 0.05:
		print("risque")
		return "RISQUE"
	robot.speedl([0,0,-speed,0,0,0], 1,maxtime)

def moveFront(robot, maxtime, speed=0.05):
	if speed > 0.05:
		print("risque")
		return "RISQUE"
	robot.speedl([0,-speed, 0,0,0,0], 1,maxtime)

def moveRight(robot, maxtime, speed=0.05):
	if speed > 0.05:
		print("risque")
		return "RISQUE"
	robot.speedl([-speed,0, 0,0,0,0], 1,maxtime)


if __name__ == "__main__":
	myVentouse = Ventouz("ventouz", 115200)
	robot = urx_modif.Robot(ip)
	print("Demarrage test")
	motorAngles = [-1.5708401838885706, -1.5718987623797815, -1.5718987623797815, 4.71239, 1.5718987623797815, 6.283233244080542]
	myVentouse.open(1)
	moveDown(robot, 100, 0.01)	
	myVentouse.waitForContact(stop, robot)
	myVentouse.close(1)
	time.sleep(1)
	robot.movej(motorAngles, acc=1, vel=0.5)
	myVentouse.open(1)
	robot.close()
