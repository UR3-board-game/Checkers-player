int pin_out_pump = 2;
int pin_in_sensor = 3;


//IMPORTANT :
//Activer la fin de ligne ou envoyer un '\n' a la fin de chaque message

void setup()
{
  pinMode(pin_out_pump, OUTPUT);
  pinMode(pin_in_sensor, INPUT);
  
  Serial.begin(115200);
}
char* name = "ventouz";
char serialBuffer[256];
char previous_sensor_state = 1;
char sensor_state = 0;
char endChar = '\n';
int i = 0;

long time = 0;
void loop()
{
  
  /*--- Partie capteur ---*/
  //On examine l'état du caopteur
  //En cas de changement, on attend 10ms
  //si à la fin de ces 10ms, y'a encore le changement
  //on le transmet, sinon c'est du bruit ou un rebond
  // => C'est une sorte de passe bas

  sensor_state = digitalRead(pin_in_sensor);
  if(sensor_state != previous_sensor_state && time == 0)
    time = millis();
  if(time != 0 && millis()-time > 10)
  {
    time = 0;
    if(sensor_state != previous_sensor_state)
      Serial.println((int)sensor_state);
    previous_sensor_state = sensor_state;
  }

  /*--- Partie réception ---*/
  // Permet de recevoir des sortes de commandes.
  // 1 : allume la pompe
  // 0 : éteint la pompe
  // # : commande spéciale (plusieurs caracteres
  // |=> #autocheck : renvoie l'état (et les problèmes si problemes il y a)
  if(Serial.available())
  {
   char receivedChar = Serial.read();
   if(receivedChar != endChar){
     serialBuffer[i++] = receivedChar;
 }
   else
   {

     serialBuffer[i] = '\0';  
     i = 0;
     switch(serialBuffer[0])
     {
      case '1':
         digitalWrite(pin_out_pump, HIGH);
      break;
      case '0':
         digitalWrite(pin_out_pump, LOW);
      break;
      case '?':
         Serial.println((int)digitalRead(pin_in_sensor));
      break;
      case '#':
        if(strcmp(serialBuffer, "#autocheck") == 0)
            Serial.println("AOK");
        else if(strcmp(serialBuffer, "#hello") == 0)
            Serial.println(name);
      break;
      default:
        Serial.print(serialBuffer);
      break;
     }
    }
   }

}
