import serial
import time

#Controle de la ventouse
class SuctionCup:

	def __init__(serialName = '/dev/ttyACM0') :
		self.SerialLink = serial.Serial(serialName, 115200, timeout=.1)
		time.sleep(1)
	# *--- Private ---*
	#	Note : Il n'est pas utile d'appeler ces fonctions de l'extérieur

	def send(message):
		self.SerialLink.write(message)
		self.SerialLink.flush()
		self.SerialLink.flushOutput()

	def receive():
		received = self.SerialLink.readline()
		self.SerialLink.flushInput()
		return received

	# *--- Public ---*

	#	Active ou non l'aspiration
	#	Input : booleen
	def succ(state):
		if state:
			self.send("1")
		else:
			self.send("0")

	#	Active ou non le souflage
	#	Input : booleen
	#	Note : 	- Pas implémenté sur le hardware
	#			- Pour relacher une piece, il suffit d'arreter de succ
	def unsucc(state)
		print("Hardware pas implémenté")

	#	Permet de savoir si le capteur de contact de la pince touche
	#	quelque chose
	#	Output : booleen
	def checkContact():
		return self.receive()

	#	Demande a la pince des informations sur elle même
	#	Permet de vérifier son état
	#	Peut être un peu long
	#	Output : String 
	def autoCheck():
		self.send("#autocheck")
		msg = ""
		while msg == "":
			msg = self.receive()
		return msg
	}
