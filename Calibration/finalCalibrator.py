#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import glob
import sys
import time

#Calibrateur de caméra

# *---------- Classe ----------*

class CheckerBoardCalibration:
    
    def __init__(self, checkerBoardFormat, videoCaptureInputName = 0):
        #Quasi constantes
        self.checkerBoardFormat = checkerBoardFormat
        self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        self.camera = cv2.VideoCapture(videoCaptureInputName)
        if not self.camera.isOpened():
            print("Impossible d'ouvrir la caméra. Vérifiez le branchement.")

        #To be filled
        self.objpoints = []
        self.imgpoints = []
        self.corners = []
        self.gray = None
        self.image = None

    def getCurrentPicture(self):
        return self.image

    #Récupère une image, vérifie qu'elle contient un échiquier et récupère l'échiquier
    def getCheckerboardPicture(self):
        if not self.camera.isOpened():
            return -3
        ret, self.image = self.camera.read()
        if ret:
            self.gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
            ret, self.corners = cv2.findChessboardCorners(self.gray, self.checkerBoardFormat, None)
            if ret:
                return 0
            return -2#N'a pas pu trouver d'échiquier
        return -1 #N'a pas pu récupérer l'image

    #Permet de récupérer la correspondance entre les points 2D et les points "3D".
    #Quand utilisée plusieurs fois, plusieurs correspondances sont créées, augmentant
    #la qualité de la calibration
    def useCalibrationPicture(self):
        if self.corners == []:
            return -1
        objp = np.zeros((self.checkerBoardFormat[0]*self.checkerBoardFormat[1],3), np.float32)
        objp[:,:2] = np.mgrid[0:self.checkerBoardFormat[0],0:self.checkerBoardFormat[1]].T.reshape(-1,2)
        imgpcorners = cv2.cornerSubPix(self.gray,self.corners,(11,11),(-1,-1),self.criteria)
        self.objpoints.append(objp)
        self.imgpoints.append(imgpcorners)
        return 0


    #Permet d'obtenir la calibration
    #Il est nécéssaire d'avoir utilisé getObjPoints au moins une fois avant
    def getCalibration(self):
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(self.objpoints, self.imgpoints, self.gray.shape[::-1], None, None)
        return ret, mtx, dist, rvecs, tvecs


    def end(self):
        cv2.destroyAllWindows()
        self.camera.release()
    
    #Superpose un échiquier virtuel à l'image réelle
    def drawCheckerboard (self):
        img = cv2.drawChessboardCorners(self.image, self.checkerBoardFormat, self.corners,True)
        cv2.imshow('img',img)

# *---------- Calibration manuelle ----------*

checkerBoardFormat = (9,9)
if __name__ == '__main__':

    #Initialisation 
    calibrationFile = open("calibrationFile.txt", "wb")
    myCalibrator = CheckerBoardCalibration((9,9))
    nbOfSuccessNeeded = 5  #Combien d'image de l'échiquier pour la calibration?

    print("Veuillez placer un damier de 10x10 dans le champ de la caméra")

    while nbOfSuccessNeeded > 0:
        if myCalibrator.getCheckerboardPicture() == 0:
            nbOfSuccessNeeded -= 1
            myCalibrator.useCalibrationPicture()
            print("Found ! ")
            time.sleep(1)
        cv2.imshow('Image avant calibration', myCalibrator.getCurrentPicture())
        #Le check de cv2.waitKey(1) est INDISPENSABLE quand on lit le flux de la caméra.
        #Sans ça, ça marche pas du tout.
        #En cas de bugs curieux sur le programme, vérifier AVANT TOUT que ça y est.
        if cv2.waitKey(1) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            sys.exit()

    myCalibrator.end()
    ret, mtx, dist, rvecs, tvecs = myCalibrator.getCalibration()
    myCalibrator.drawCheckerboard()

    print("Calibration terminée :")
    print(ret)
    print(mtx)
    print(dist)
    print(rvecs)
    print(tvecs)
    print("Si l'image est incorrecte, quittez avec q et relancez le programme")
    print("Si l'image est correcte, appuyez sur o pour valider et quitter.")
    
    #Ici on observe les résultats de la calibration
    camera = cv2.VideoCapture()
    camera.open(0)

    while(True):
        if not camera.isOpened():
            print ("wtf")
        
        ret, img = camera.read()

        h,  w = img.shape[:2]
        newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0,(w,h))

        mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)
        dst = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)

        x,y,w,h = roi
        
        cv2.imshow('Image corrigee', dst)
        pressed = cv2.waitKey(1) & 0xFF
        if pressed == ord('o'):
            np.savez(calibrationFile,ret,mtx,dist,rvecs,tvecs,False)
            cv2.destroyAllWindows()
            camera.release()
            sys.exit()                               
                    
        if pressed == ord('q'):
            cv2.destroyAllWindows()
            camera.release()
            sys.exit()
        
