#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import glob
import sys
import time


cv2.namedWindow('Image avant calibration', cv2.WINDOW_AUTOSIZE) 
camera = cv2.VideoCapture()
camera.open(0)
if not camera.isOpened():
    print("Impossible d'ouvrir la caméra. Vérifiez le branchement.")

while True:
	ret, img = camera.read()
	if ret :
		cv2.imshow('Image avant calibration', img)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		cv2.destroyAllWindows()
		sys.exit()

